<?php
session_start();
@mysql_connect("localhost","admin","Password") or die (mysql_error());
@mysql_select_db("computershopv0.1") or die (mysql_error());

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Index</title>
	<link rel="stylesheet"
	href="bootstrap-3.3.6-dist/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="styles/checkoutStyle.css">
</head>
<?php
$sum = 0;
function fetch_build($buildID) {
	global $sum;
	$str = '';
	$sql = "SELECT tbl_builds.bu_ID, tbl_parts.part_name, tbl_suppliers.sup_name, tbl_parts.part_price FROM tbl_builds INNER JOIN tbl_parts INNER JOIN tbl_suppliers WHERE tbl_builds.bu_ID = '$buildID' AND tbl_builds.part_ID = tbl_parts.part_ID AND tbl_suppliers.sup_ID = tbl_parts.sup_ID";
	$result = mysql_query($sql) or die ( mysql_error());
	for ( $x = 0 ; $x < mysql_num_rows($result) ; $x++ ) {
		$part_name = mysql_result($result,$x,"part_name");
		$sup_name = mysql_result($result,$x,"sup_name");
		$price = mysql_result($result,$x,"part_price");
		$sum += $price;
		$str .= '<a href="#" class="list-group-item">
			<form method="post" action="cart_update.php">
			<h4 class="list-group-item-heading"><div>'.$part_name.'</div><div class="pull-right">'.$price.' THB</div></h4>
			<p class="list-group-item-text"><b>Supplier: </b>'.$sup_name.'</p>
			</form>
			</a>';
	}
	return $str;
}

if(isset($_POST['checkout'])) {
	$cus_ID = $_SESSION['cus_ID'];
	$bu_ID = $_SESSION['buildID'];
	$chk_sql = "INSERT INTO `tbl_orders` VALUES (DEFAULT,'$cus_ID','$bu_ID')";
	mysql_query($chk_sql) or die (mysql_error());
	unset($_SESSION['buildID']);
	header('Location: main.php');
}

?>
<body>
	<div class="col-md-offset-2 col-md-4">
		<div class="row"><h1>Your build:</h1></div>
		<div class="row">
			<div class="list-group" id="cart">
				<div id="buildList">
				<?php
					if(isset($_SESSION['buildID'])){
						 echo fetch_build($_SESSION['buildID']);
					}
				?>
				</div>
				<a href="#" class="list-group-item active">
				<form method="post" action="cart_update.php">
				<h4 class="list-group-item-heading">Total : <span class="pull-right"><?php echo $sum; ?> THB</span></h4>
				</form>
				</a>
			</div>
		</div>
		<form action="checkout.php" method="post">
			<input type="hidden" name="command" value="remove">
		<button type="submit" class="btn btn-block btn-success" name="checkout">Order</button>
		<a href="main.php" class="btn btn-danger btn-block">Cancel</a>
		</form>

		</div>
	</body>
	</html>
