<?php
session_start();
@mysql_connect("localhost","admin","Password") or die (mysql_error());
@mysql_select_db("computershopv0.1") or die (mysql_error());

?>
<?php

$loginMsg = '';
function verifyLogin($id, $pass) {
	global $loginMsg;
	$sql = "SELECT cus_user, cus_pass, cus_ID, cus_name FROM tbl_customers WHERE cus_user = '$id'";
	$result = mysql_query($sql) or die (mysql_error());
	if( mysql_num_rows($result) > 0 ){
		$id_tbl = mysql_result($result,0,"cus_user")or die (mysql_error());
		$pass_tbl = mysql_result($result,0,"cus_pass")or die (mysql_error());
		$cus_ID = mysql_result($result,0,"cus_ID") or die(mysql_error());
		$name = mysql_result($result,0,"cus_name");
		if( $id == $id_tbl && $pass == $pass_tbl ) {
			$_SESSION['loggedin'] = true;
			$_SESSION['id'] = $id;
			$_SESSION['cus_ID'] = $cus_ID;
			$_SESSION['name'] = $name;
			$loginMsg = '<div class="alert alert-success">Welcome!</div>';
			header("Location: main.php");
		}
		else {
			$loginMsg = '<div class="alert alert-danger">Wrong ID/Password.</div>';
		}
	} else {
		$loginMsg = '<div class="alert alert-danger">Wrong ID/Password.</div>';
	}
}
if(isset($_POST['login']) && !empty($_POST['loginID']) && !empty($_POST['loginPW'])) {

	$id = $_POST['loginID'];
	$pw = $_POST['loginPW'];
	verifyLogin($id, $pw);
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Index</title>
	<link rel="stylesheet"
	href="bootstrap-3.3.6-dist/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="styles/indexStyle.css">
</head>
<body>
	<div class="col-md-offset-3 col-sm-offset-3 col-xs-offset-3">
		<div class="row">

			<div class="container vcenter" id="loginBox">
				<div class="row" id="notifyLbl"><center>Please login</center></div>
				<div class="row" id="alertLbl"><center>
					<?php echo $loginMsg; ?>
				</center></div>
				<form action="index.php" method="post">
					<div class="form-group">
						<label for="loginID">ID</label>
						<input type="id" class="form-control" id="loginID" name="loginID" placeholder="ID...">
					</div>
					<div class="form-group">
						<label for="loginPassword">Password</label>
						<input type="password" class="form-control" id="loginPassword" name="loginPW" placeholder="Password...">
					</div>
					<button type="submit" class="btn btn-info" id="loginBtn" name="login">Login</button>
					<a href="register.php" class="btn btn-primary" id="regisBtn">Register</a>
					<a href="adminLogin.php" class="btn btn-success" id="adminLogin">Admin</a>
				</form>

			</div>
		</div>
	</div>
	<script src="jquery-2.2.3.min.js"></script>

	<script type="text/javascript" src="scripts/indexScript.js"></script>

</body>
</html>
