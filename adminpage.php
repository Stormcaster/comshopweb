<?php
   session_start();
   @mysql_connect("localhost","admin","Password") or die (mysql_error());
   @mysql_select_db("computershopv0.1") or die (mysql_error()); ?>
<?php
if(!isset($_SESSION['adminloggedin'])) {
	header('Refresh: 0; URL = index.php');
}

function fetch_cus() {
	$str = '';
	$result = mysql_query("SELECT * FROM tbl_customers") or die (mysql_error());
	for ( $x = 0 ; $x < mysql_num_rows($result) ; $x++ ) {
	$cus_ID = mysql_result($result,$x,"cus_ID");
	$cus_name = mysql_result($result,$x,"cus_name");
	$cus_surname = mysql_result($result,$x,"cus_surname");
	$cus_user = mysql_result($result,$x,"cus_user");
	$cus_pass = mysql_result($result,$x,"cus_pass");
	$cus_address = mysql_result($result,$x,"cus_address");
	$cus_num = mysql_result($result,$x,"cus_num");
		$str .= '
	<a href="#" class="list-group-item">
	<h4 class="list-group-item-heading">'.$cus_ID.'</h4>
	<p class="list-group-item-text"><b>name: </b>'.$cus_name.'</p>
	<p class="list-group-item-text"><b>surname: </b>'.$cus_surname.'</p>
	<p class="list-group-item-text"><b>user: </b>'.$cus_user.'</p>
	<p class="list-group-item-text"><b>password: </b>'.$cus_pass.'</p>
	<p class="list-group-item-text"><b>address: </b>'.$cus_address.'</p>
	<p class="list-group-item-text"><b>number: </b>'.$cus_num.'</p>
	</a>';
   }
   return $str;
}
function fetch_sup() {
   $str = '';
   $result = mysql_query("SELECT * FROM tbl_suppliers;") or die (mysql_error());
   for ( $x = 0 ; $x < mysql_num_rows($result) ; $x++ ) {
	$sup_ID = mysql_result($result,$x,"sup_ID");
	$sup_name = mysql_result($result,$x,"sup_name");
	$sup_address = mysql_result($result,$x,"sup_address");
	$sup_num = mysql_result($result,$x,"sup_num");
		$str .= '
	<a href="#" class="list-group-item">
	<h4 class="list-group-item-heading">'.$sup_ID.'</h4>
	<p class="list-group-item-text"><b>name: </b>'.$sup_name.'</p>
	<p class="list-group-item-text"><b>address: </b>'.$sup_address.'</p>
	<p class="list-group-item-text"><b>number: </b>'.$sup_num.'</p>
	</a>';
	}
	return $str;
}
function fetch_cat() {
   $str = '';
   $result = mysql_query("SELECT * FROM tbl_categories;") or die (mysql_error());
   for ( $x = 0 ; $x < mysql_num_rows($result) ; $x++ ) {
	$cat_ID = mysql_result($result,$x,"cat_ID");
	$cat_name = mysql_result($result,$x,"cat_name");
	$str .= '
	<a href="#" class="list-group-item">
	<h4 class="list-group-item-heading">'.$cat_ID.'</h4>
	<p class="list-group-item-text"><b>category: </b>'.$cat_name.'</p>
	</a>';
   }
   return $str;
}
function fetch_parts() {
	$str = '';
	$result = mysql_query("SELECT * FROM tbl_parts INNER JOIN tbl_suppliers INNER JOIN tbl_categories WHERE tbl_parts.sup_ID = tbl_suppliers.sup_ID AND tbl_parts.cat_ID = tbl_categories.cat_ID;") or die (mysql_error());
	for ( $x = 0 ; $x < mysql_num_rows($result) ; $x++ ) {
	$part_ID = mysql_result($result,$x,"part_ID");
	$cat_name = mysql_result($result,$x,"cat_name");
	$part_name = mysql_result($result,$x,"part_name");
   $sup_name = mysql_result($result,$x,"sup_name");
	$part_imp_date = mysql_result($result,$x,"part_imp_date");
	$part_price = mysql_result($result,$x,"part_price");
	$part_amt = mysql_result($result,$x,"part_amt");
		$str .= '
	<a href="#" class="list-group-item">
	<h4 class="list-group-item-heading">'.$part_ID.'</h4>
	<p class="list-group-item-text"><b>part name: </b>'.$part_name.'</p>
	<p class="list-group-item-text"><b>category: </b>'.$cat_name.'</p>
	<p class="list-group-item-text"><b>suppplier name: </b>'.$sup_name.'</p>
	<p class="list-group-item-text"><b>import date: </b>'.$part_imp_date.'</p>
	<p class="list-group-item-text"><b>price: </b>'.$part_price.'</p>
	<p class="list-group-item-text"><b>amount left: </b>'.$part_amt.'</p>
	</a>';
   }
   return $str;
}
function fetch_builds() {
   $str = '';
   $result = mysql_query("SELECT * FROM tbl_builds INNER JOIN tbl_parts WHERE tbl_builds.part_ID = tbl_parts.part_ID;") or die (mysql_error());
   for ( $x = 0 ; $x < mysql_num_rows($result) ; $x++ ) {
	$bu_ID = mysql_result($result,$x,"bu_ID");
	$part_name = mysql_result($result,$x,"part_name");
	$str .= '
	<a href="#" class="list-group-item">
	<h4 class="list-group-item-heading">'.$bu_ID.'</h4>
	<p class="list-group-item-text"><b>part name: </b>'.$part_name.'</p>
	</a>';
   }
   return $str;
}
function fetch_orders() {
   $str = '';
   $result = mysql_query("SELECT * FROM tbl_orders;") or die (mysql_error());
   for ( $x = 0 ; $x < mysql_num_rows($result) ; $x++ ) {
	$ord_ID = mysql_result($result,$x,"ord_ID");
	$cus_ID = mysql_result($result,$x,"cus_ID");
	$bu_ID = mysql_result($result,$x,"bu_ID");
	$str .= '
	<a href="#" class="list-group-item">
	<h4 class="list-group-item-heading">'.$ord_ID.'</h4>
	<p class="list-group-item-text"><b>cus_ID: </b>'.$cus_ID.'</p>
	<p class="list-group-item-text"><b>bu_ID: </b>'.$bu_ID.'</p>
	'.get_part_name_list_from_bu_ID($bu_ID).'
	</a>';
   }
   return $str;
}
function get_part_name_list_from_bu_ID($bu_ID) {
   $tmp = '';
   $result = mysql_query("SELECT tbl_parts.part_name FROM tbl_builds INNER JOIN tbl_parts WHERE tbl_builds.bu_ID = '$bu_ID' AND tbl_builds.part_ID = tbl_parts.part_ID;");
   for($y = 0 ; $y < mysql_num_rows($result) ; $y++ ){
	$part_name = mysql_result($result,$y,"part_name");
	$tmp .= '<p class="list-group-item-text"><b>part_name: </b>'.$part_name.'</p>';
   }
   return $tmp;
}
function fetch_adm() {
	$str = '';
	$result = mysql_query("SELECT * FROM tbl_admins") or die (mysql_error());
	for ( $x = 0 ; $x < mysql_num_rows($result) ; $x++ ) {
	$adm_ID = mysql_result($result,$x,"adm_ID");
	$adm_name = mysql_result($result,$x,"adm_name");
	$adm_user = mysql_result($result,$x,"adm_user");
	$adm_pass = mysql_result($result,$x,"adm_pass");
	$str .= '
	<a href="#" class="list-group-item">
	<h4 class="list-group-item-heading">'.$adm_ID.'</h4>
	<p class="list-group-item-text"><b>cus_name: </b>'.$adm_name.'</p>
	<p class="list-group-item-text"><b>cus_user: </b>'.$adm_user.'</p>
	<p class="list-group-item-text"><b>cus_password: </b>'.$adm_pass.'</p>
	</a>';
   }
   return $str;
}
$cus_add_msg = '';
if(isset($_POST['cus_add_btn'])) {
   $cus_name = $_POST['cus_name'];
   $cus_surname = $_POST['cus_surname'];
   $cus_user = $_POST['cus_user'];
   $cus_password = $_POST['cus_password'];
   $cus_address = $_POST['cus_address'];
   $cus_num = $_POST['cus_num'];
   if( mysql_num_rows(mysql_query("SELECT * FROM tbl_customers WHERE tbl_customers.cus_user = '$cus_user';") ) == 0 ) {
      $sql = "INSERT INTO `tbl_customers` VALUES (DEFAULT, '$cus_name', '$cus_surname', '$cus_user', '$cus_password', '$cus_address', '$cus_num');";
      mysql_query($sql) or die (mysql_error());
   } else {
      $cus_add_msg = "Your ID already exists. Please choose a new one.";
   }
}
$sup_add_msg = '';
if(isset($_POST['sup_add_btn'])) {
   $sup_name = $_POST['sup_name'];
   $sup_address = $_POST['sup_address'];
   $sup_num = $_POST['sup_num'];
   if( mysql_num_rows(mysql_query("SELECT * FROM tbl_suppliers WHERE tbl_suppliers.sup_name = '$sup_name';") ) == 0 ) {
      $sql = "INSERT INTO `tbl_suppliers` VALUES (DEFAULT, '$sup_name', '$sup_address', '$sup_num');";
      mysql_query($sql) or die (mysql_error());
   } else {
      $sup_add_msg = "Your company already exists. Please check for existing one.";
   }
}
$cat_add_msg = '';
if(isset($_POST['cat_add_btn'])) {
   $cat_name = $_POST['cat_name'];
   if( mysql_num_rows(mysql_query("SELECT * FROM tbl_categories WHERE tbl_categories.cat_name = '$cat_name';") ) == 0 ) {
      $sql = "INSERT INTO `tbl_categories` VALUES (DEFAULT, '$cat_name');";
      mysql_query($sql) or die (mysql_error());
   } else {
      $cat_add_msg = "Your category already exists. Please check for exist one.";
   }
}
$part_add_msg = '';
if(isset($_POST['part_add_btn'])) {
   $part_cat_ID = $_POST['part_cat_ID'];
   $part_name = $_POST['part_name'];
   $part_sup_ID = $_POST['part_sup_ID'];
   $part_imp_date = $_POST['part_imp_date'];
   $part_imp_time = $_POST['part_imp_time'];
   $part_price = $_POST['part_price'];
   $part_amt = $_POST['part_amt'];
   if( mysql_num_rows(mysql_query("SELECT * FROM tbl_parts WHERE tbl_parts.part_name = '$part_name' AND tbl_parts.sup_ID = '$part_sup_ID';") ) == 0 ) {
      $sql = "INSERT INTO `tbl_parts` VALUES (DEFAULT, '$part_cat_ID', '$part_name', '$part_sup_ID', '$part_imp_date $part_imp_time', '$part_price', '$part_amt');";
      mysql_query($sql) or die (mysql_error());
   } else {
      $part_add_msg = "Your part already exists. Please check exist one.";
   }
}
$adm_add_msg = '';
if(isset($_POST['adm_add_btn'])) {
   $adm_name = $_POST['adm_name'];
   $adm_user = $_POST['adm_user'];
   $adm_pass = $_POST['adm_pass'];
   if( mysql_num_rows(mysql_query("SELECT * FROM tbl_admins WHERE tbl_admins.adm_user = '$adm_user';") ) == 0 ) {
      $sql = "INSERT INTO `tbl_admins` VALUES (DEFAULT, '$adm_name', '$adm_user', '$adm_pass');";
      mysql_query($sql) or die (mysql_error());
   } else {
      $adm_add_msg = "Your ID already exists. Please choose a new one.";
   }
}
?>
<!DOCTYPE html>
<style>
.list_item {
   max-height: 450px;
   overflow-y: auto;
}
</style>
<head>
	<meta charset="utf-8">
	<title>Admin</title>
	<link rel="stylesheet" href="bootstrap-3.3.6-dist/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="styles/adminStyle.css">
</head>
<body>
<div class="container">
	<span class="pull-right" id="accountPanel">
		<span href="adminpage.php" class="btn btn-primary" id="online"><?php echo "Logged in as: " . $_SESSION['adm_name']; ?></span>
		<a href="logout.php" class="btn btn-warning" id="logoutBtn">Log Out</a>
		</span>
	<h1>CUSTOMERS</h1>
		

	<div class="col-md-6">
		<div class="list_item">
			<div class="list-group">
				<?php echo fetch_cus(); ?>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<form action="adminpage.php" method="post">
		<div class="form-group">
         <div class="row" id="alertLbl"><center>
            <?php echo $cus_add_msg; ?>
         </center></div>
			<label for="name">Name</label>
			<input type="id" class="form-control" id="name" name="name" placeholder="Name...">
			</div>
			<div class="form-group">
			<label for="surname">Surname</label>
			<input type="id" class="form-control" id="surname" name="surname" placeholder="Surname...">
			</div>
			<div class="form-group">
			<label for="address">Address</label>
			<input type="id" class="form-control" id="address" name="address" placeholder="Address">
			</div>
			<div class="form-group">
			<label for="number">Phone Number</label>
			<input type="id" class="form-control" id="number" name="num" placeholder="Phone No...">
			</div>
			<div class="form-group">
			<label for="user">User ID</label>
			<input type="id" class="form-control" id="user" name="user" placeholder="ID...">
			</div>
			<div class="form-group">
			<label for="password">Password</label>
			<input type="password" class="form-control" id="password" name="password" placeholder="Password...">
		</div>
		<button type="submit" class="btn btn-info" name="cus_add_btn">Add</button>
		</form>
	</div>
</div>
<div class="container">
	<h1>SUPPLIERS</h1>

	<div class="col-md-6">
		<div class="list_item">
			<div class="list-group">
				<?php echo fetch_sup(); ?>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<form action="adminpage.php" method="post">
			<div class="form-group">
         <div class="row" id="alertLbl"><center>
            <?php echo $sup_add_msg; ?>
         </center></div>
			<label for="sup_name">Supplier Name</label>
			<input type="id" class="form-control" id="sup_name" name="sup_name" placeholder="Supplier name...">
			</div>
			<div class="form-group">
			<label for="sup_addr">Address</label>
			<input type="id" class="form-control" id="sup_addr" name="sup_address" placeholder="Address...">
			</div>
			<div class="form-group">
			<label for="sup_phone">Phone No.</label>
			<input type="id" class="form-control" id="sup_phone" name="sup_num" placeholder="Phone number...">
			</div>
			<button type="submit" class="btn btn-info" name="sup_add_btn">Add</button>
		</form>
	</div>
</div>
<div class="container">
	<h1>CATEGORIES</h1>

	<div class="col-md-6">
		<div class="list_item">
			<div class="list-group">
				<?php echo fetch_cat(); ?>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<form action="adminpage.php" method="post">
			<div class="form-group">
            <div class="row" id="alertLbl"><center>
               <?php echo $cat_add_msg; ?>
            </center></div>
				<label for="cat_name">Category Name</label>
				<input type="id" class="form-control" id="cat_name" name="cat_name" placeholder="Category Name...">
			</div>
			<button type="submit" class="btn btn-info" name="cat_add_btn">Add</button>
		</form>
	</div>
</div>
<div class="container">
	<h1>PARTS</h1>

	<div class="col-md-6">
		<div class="list_item">
			<div class="list-group">
				<?php echo fetch_parts(); ?>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<form action="adminpage.php#PARTS" method="post">
         <div class="row" id="alertLbl"><center>
            <?php echo $part_add_msg; ?>
         </center></div>
			<div class="form-group">
			<label for="part_catID">Category ID</label>
			<input type="id" class="form-control" id="part_catn_name" name="part_cat_ID" placeholder="Category ID...">
			</div>
			<div class="form-group">
			<label for="part_name">Part Name</label>
			<input type="id" class="form-control" id="part_name" name="part_name" placeholder="Part Name...">
			</div>
			<div class="form-group">
			<label for="part_supID">Supplier ID</label>
			<input type="id" class="form-control" id="part_supID" name="part_sup_ID" placeholder="Supplier ID">
			</div>
			<div class="form-group">
			<label for="part_imp_date">Import Date & Time</label>
			<input type="date" class="form-control" id="part_imp_date" name="part_imp_date" placeholder="2016-04-29 08:00:00....">
			<input type="time" class="form-control" id="part_imp_time" name="part_imp_time" placeholer="Time...">
			</div>
			<div class="form-group">
			<label for="part_price">Price</label>
			<input type="id" class="form-control" id="part_price" name="part_price" placeholder="Price...">
			</div>
			<div class="form-group">
			<label for="part_amt">Amount</label>
			<input type="text" class="form-control" id="part_amt" name="part_amt" placeholder="amount...">
			</div>
			<button type="submit" class="btn btn-info" name="part_add_btn">Add</button>
		</form>
	</div>
</div>
<div class="container">
	<h1>ADMINS</h1>

	<div class="col-md-6">
		<div class="list_item">
			<div class="list-group">
				<?php echo fetch_adm(); ?>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<form action="adminpage.php" method="post">
		<div class="form-group">
         <div class="row" id="alertLbl"><center>
            <?php echo $adm_add_msg; ?>
         </center></div>
			<label for="adm_name">Admin Name</label>
			<input type="id" class="form-control" id="adm_name" name="adm_name" placeholder="Admin Name...">
			</div>
			<div class="form-group">
			<label for="adm_user">Admin User ID</label>
			<input type="id" class="form-control" id="adm_user" name="adm_user" placeholder="Admin User ID...">
			</div>
			<div class="form-group">
			<label for="adm_pass">Password</label>
			<input type="password" class="form-control" id="adm_pass" name="adm_pass" placeholder="Password">
			</div>
			<button type="submit" class="btn btn-info" name="adm_add_btn">Add</button>
			</form>
	</div>
</div>
<div class="container">
   <div class="col-md-6">
   <h1>BUILD</h1>
   <div class="list_item">
			<div class="list-group">
	<?php echo fetch_builds(); ?>
	</div>
   </div>
   </div>
   <div class="col-md-6">
   <h1>ORDERS</h1>
   <div class="list_item">
			<div class="list-group">
	<?php echo fetch_orders(); ?>
	</div>
   </div>
   </div>
</div>

</body>
