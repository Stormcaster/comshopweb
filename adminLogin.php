<?php
session_start();
@mysql_connect("localhost","admin","Password") or die (mysql_error());
@mysql_select_db("computershopv0.1") or die (mysql_error());

?>
<?php

$loginMsg = '';
function verifyLogin($id, $pass) {
	global $loginMsg;
	$sql = "SELECT adm_user, adm_pass, adm_ID, adm_name FROM tbl_admins WHERE adm_user = '$id'";
	$result = mysql_query($sql) or die (mysql_error());
	if( mysql_num_rows($result) > 0 ){
		$adm_user = mysql_result($result,0,"adm_user")or die (mysql_error());
		$adm_pass = mysql_result($result,0,"adm_pass")or die (mysql_error());
		$adm_ID = mysql_result($result,0,"adm_ID") or die(mysql_error());
		$adm_name = mysql_result($result,0,"adm_name") or die (mysql_error());
		if( $id == $adm_user && $pass == $adm_pass ) {
			$_SESSION['adminloggedin'] = true;
			$_SESSION['id'] = $id;
			$_SESSION['adm_name'] = $adm_name;
			$_SESSION['adm_ID'] = $adm_ID;
			$loginMsg = '<div class="alert alert-success">Welcome!</div>';
			header("Location: adminpage.php");
		}
		else {
			$loginMsg = '<div class="alert alert-danger">Wrong ID/Password.</div>';
		}
	} else {
		$loginMsg = '<div class="alert alert-danger">Wrong ID/Password.</div>';
	}
}
if(isset($_POST['login']) && !empty($_POST['loginID']) && !empty($_POST['loginPW'])) {

	$id = $_POST['loginID'];
	$pw = $_POST['loginPW'];
	verifyLogin($id, $pw);
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Index</title>
	<link rel="stylesheet"
	href="bootstrap-3.3.6-dist/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="styles/indexStyle.css">
</head>
<body>
	<div class="col-md-offset-3 col-sm-offset-3 col-xs-offset-3">
		<div class="row">

			<div class="container vcenter" id="loginBox">
				<div class="row" id="notifyLbl"><center>Administrator page</center></div>
				<div class="row" id="alertLbl"><center>
					<?php echo $loginMsg; ?>
				</center></div>
				<form action="adminLogin.php" method="post">
					<div class="form-group">
						<label for="loginID">ID</label>
						<input type="id" class="form-control" id="loginID" name="loginID" placeholder="ID...">
					</div>
					<div class="form-group">
						<label for="loginPassword">Password</label>
						<input type="password" class="form-control" id="loginPassword" name="loginPW" placeholder="Password...">
					</div>
					<button type="submit" class="btn btn-info" id="loginBtn" name="login">Login</button>
					<a href="index.php" class="btn btn-danger" id="backBtn">Back</a>
				</form>

			</div>
		</div>
	</div>
	<script src="jquery-2.2.3.min.js"></script>

	<script type="text/javascript" src="scripts/indexScript.js"></script>

</body>
</html>
