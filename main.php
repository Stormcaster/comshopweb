<?php
session_start();
@mysql_connect("localhost","admin","Password") or die (mysql_error());
@mysql_select_db("computershopv0.1") or die (mysql_error());
if(!isset($_SESSION['loggedin'])) {
	header('Refresh: 0; URL = index.php');
}
/*define*/
$cat_cpu = '301';
$cat_mb ='302';
$cat_ram = '303';
$cat_grap = '304';
$cat_pow = '305';
$cat_util = '306';
$cat_case = '307';
$cat_acc = '308';

function fetch_product_by_catID($catID) {
	$str = '';
	$sql = "SELECT tbl_parts.part_name,tbl_parts.part_ID,tbl_suppliers.sup_name, tbl_parts.part_price, tbl_parts.part_amt FROM tbl_parts INNER JOIN tbl_suppliers WHERE tbl_parts.cat_ID = '$catID' AND tbl_suppliers.sup_ID = tbl_parts.sup_ID";
	$result = mysql_query($sql) or die (mysql_error());
	for ( $x = 0 ; $x < mysql_num_rows($result) ; $x++ ) {
		$part_name = mysql_result($result,$x,"part_name");
		$sup_name = mysql_result($result,$x,"sup_name");
		$part_ID = mysql_result($result,$x,"part_ID");
		$part_price = mysql_result($result,$x,"part_price");
		$amount = mysql_result($result,$x,"part_amt");
		$str .= '<a href="#" class="list-group-item">
			<form method="post" action="cart_update.php">
			<div class="list-group-item-heading h4"><span>'.$part_name.'</span><span class="pull-right">'.$part_price.' THB</span></div>
			<p class="list-group-item-text"><b>Supplier: </b>'.$sup_name.'</p>
			<input type="hidden" name="command" value="add">
			<input type="hidden" name="partID" value=' . $part_ID . '>
			<p class="h5"><button type="submit" class="btn btn-info"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span></button>
			<span class="pull-right">Remaining: '.$amount.'</span></p>
			</form>
			</a>';
	}
	return $str;
}
$sum = 0;
function fetch_build($buildID) {
	global $sum;
	$str = '';
	$sql = "SELECT tbl_builds.bu_ID,tbl_builds.part_ID, tbl_parts.part_name, tbl_suppliers.sup_name, tbl_parts.part_price FROM tbl_builds INNER JOIN tbl_parts INNER JOIN tbl_suppliers WHERE tbl_builds.bu_ID = '$buildID' AND tbl_builds.part_ID = tbl_parts.part_ID AND tbl_suppliers.sup_ID = tbl_parts.sup_ID";
	$result = mysql_query($sql) or die ( mysql_error());
	for ( $x = 0 ; $x < mysql_num_rows($result) ; $x++ ) {
		$part_name = mysql_result($result,$x,"part_name");
		$sup_name = mysql_result($result,$x,"sup_name");
		$part_ID = mysql_result($result,$x,"part_ID");
		$part_price = mysql_result($result,$x,"part_price");
		$sum += $part_price;
		$str .= '<a href="#" class="list-group-item">
			<form method="post" action="cart_update.php">
			<h4 class="list-group-item-heading">'.$part_name.'</h4>
			<p class="list-group-item-text"><b>Supplier: </b>'.$sup_name.'</p>
			<input type="hidden" name="command" value="remove">
			<input type="hidden" name="partID" value=' . $part_ID . '>

			<button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
			<span class="pull-right h4">'.$part_price.' THB</span>
			</form>
			</a>';
	}
	return $str;
}

?>
<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8">
		<title>Index</title>
		<link rel="stylesheet"
			href="bootstrap-3.3.6-dist/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="styles/mainStyle.css">
	</head>
<body>
	<div class="col-md-10 col-md-offset-1" id="topbar">
	<div class="row">
		<span>
			<img id="banner" src="resources/banner.png" style="">
		</span>
		<span class="pull-right" id="accountPanel">
		<span href="main.php" class="btn btn-primary" id="online"><?php echo "Logged in as: " . $_SESSION['name']; ?></span>
		<a href="logout.php" class="btn btn-warning" id="logoutBtn">Log Out</a>
		</span>
	</div>
	</div>
	<div class="col-md-offset-1 col-md-7">
	<div class="row"><h2>Components</h2></div>
	<?php
		$errormsg = '';
		if(isset($_SESSION['cart_upd_error'])) {
			$carterror = $_SESSION['cart_upd_error'];
			if($carterror == "noitem")
				$errormsg = '<div class="alert alert-danger" role="alert"><strong>Out of stock!</strong> We have none of that item left. Sorry.</div>';
			echo $errormsg;
			unset($_SESSION['cart_upd_error']);
		}
	?>
	<div class="row">
		<ul class="nav nav-pills nav-justified productPills" id="productTabs">
				<li class="active"><a href="#CPUPane" data-toggle="tab">CPU</a></li>
				<li><a href="#GPUPane" data-toggle="tab">Graphics</a></li>
				<li><a href="#PowerSupplyPane" data-toggle="tab">PowerSupply</a></li>
				<li><a href="#mbPane" data-toggle="tab">Mainboard</a></li>
				<li><a href="#casePane" data-toggle="tab">Case</a></li>
				<li><a href="#accPane" data-toggle="tab">Accessory</a></li>
				<li><a href="#utilPane" data-toggle="tab">Utility</a></li>
				<li><a href="#ramPane" data-toggle="tab">RAM</a></li>
		</ul>
		<div class="tab-content" id="products">
			<div class="tab-pane active" id="CPUPane">
				<div class="list-group">
					<?php echo fetch_product_by_catID($cat_cpu);?>
				</div>
			</div>
			<div class="tab-pane" id="GPUPane">
				<div class="list-group">
					<?php echo fetch_product_by_catID($cat_grap); ?>
				</div>
			</div>
			<div class="tab-pane" id="PowerSupplyPane">
				<div class="list-group">
					<?php echo fetch_product_by_catID($cat_pow); ?>
				</div>
			</div>
			<div class="tab-pane" id="mbPane">
				<div class="list-group">
					<?php echo fetch_product_by_catID($cat_mb); ?>
				</div>
			</div>
			<div class="tab-pane" id="casePane">
				<div class="list-group">
					<?php echo fetch_product_by_catID($cat_case); ?>
				</div>
			</div>
			<div class="tab-pane" id="accPane">
				<div class="list-group">
					<?php echo fetch_product_by_catID($cat_acc); ?>
				</div>
			</div>
			<div class="tab-pane" id="utilPane">
				<div class="list-group">
					<?php echo fetch_product_by_catID($cat_util); ?>
				</div>
			</div>
			<div class="tab-pane" id="ramPane">
				<div class="list-group">
					<?php echo fetch_product_by_catID($cat_ram); ?>
				</div>
			</div>
		</div>
	</div>
	</div>
	<div class="col-md-offset-1 col-md-2">
		<div class="row"><h2>Build
			<a href="checkout.php" class="btn btn-success" id="checkoutBtn">
				<?php
				if(isset($_SESSION['buildID']))
				fetch_build($_SESSION['buildID']);
				echo $sum ?> THB
				<span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>
				<span class="glyphicon glyphicon-usd" aria-hidden="true"></span>
				</a></h2>
			</div>
		<div class="row">
			<div class="list-group" id="cart">
				<?php
					if(isset($_SESSION['buildID'])){
						 echo fetch_build($_SESSION['buildID']);
					}
				?>
			</div>
		</div>
	</div>
<script src="jquery-2.2.3.min.js"></script>

<script type="text/javascript" src="scripts/mainScript.js"></script>
<script type="text/javascript" src="../bootstrap-3.3.6-dist/js/bootstrap.js"></script>

</body>
</html>
