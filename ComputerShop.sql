-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 02, 2016 at 05:08 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `computershopv0.1`
--
CREATE DATABASE IF NOT EXISTS `computershopv0.1` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `computershopv0.1`;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admins`
--

CREATE TABLE `tbl_admins` (
  `adm_ID` int(10) UNSIGNED NOT NULL,
  `adm_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `adm_user` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `adm_pass` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_admins`
--

INSERT INTO `tbl_admins` (`adm_ID`, `adm_name`, `adm_user`, `adm_pass`) VALUES
(701, 'PeaNamchai', 'adminPea', 'password'),
(702, 'ThirtyCraZyLow', 'admin3rd', 'password'),
(703, 'Poom', 'adminPoom', 'password');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_builds`
--

CREATE TABLE `tbl_builds` (
  `bu_ID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `part_ID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_builds`
--

INSERT INTO `tbl_builds` (`bu_ID`, `part_ID`) VALUES
(501, 422),
(501, 421),
(501, 420),
(502, 431);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_categories`
--

CREATE TABLE `tbl_categories` (
  `cat_ID` int(10) UNSIGNED NOT NULL,
  `cat_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_categories`
--

INSERT INTO `tbl_categories` (`cat_ID`, `cat_name`) VALUES
(301, 'CPU'),
(302, 'mainboard'),
(303, 'ram'),
(304, 'graphic card'),
(305, 'power supply'),
(306, 'utility card'),
(307, 'case'),
(308, 'accessory');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customers`
--

CREATE TABLE `tbl_customers` (
  `cus_ID` int(10) UNSIGNED NOT NULL,
  `cus_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cus_surname` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cus_user` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cus_pass` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cus_address` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cus_num` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_customers`
--

INSERT INTO `tbl_customers` (`cus_ID`, `cus_name`, `cus_surname`, `cus_user`, `cus_pass`, `cus_address`, `cus_num`) VALUES
(101, 'Woramate', 'Jumroonsilp', 'ThirtyCraZyLow', 'Password', 'Bangkok', '0899999999'),
(102, 'Pitchaya', 'Namchaisiri', 'PeaNamchai', 'Password', 'Thailand', '0999999999'),
(103, 'Admin', '', 'admin', 'password', 'Thailand', '0999999999'),
(104, 'test', 'test', 'test', 'test', 'test', 'test'),
(105, 'DB', 'DB', 'DB', 'password', 'bkk', '000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_orders`
--

CREATE TABLE `tbl_orders` (
  `ord_ID` int(10) UNSIGNED NOT NULL,
  `cus_ID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `bu_ID` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_orders`
--

INSERT INTO `tbl_orders` (`ord_ID`, `cus_ID`, `bu_ID`) VALUES
(601, 104, 501);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_parts`
--

CREATE TABLE `tbl_parts` (
  `part_ID` int(10) UNSIGNED NOT NULL,
  `cat_ID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `part_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sup_ID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `part_imp_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `part_price` decimal(9,2) NOT NULL DEFAULT '0.00',
  `part_amt` smallint(5) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_parts`
--

INSERT INTO `tbl_parts` (`part_ID`, `cat_ID`, `part_name`, `sup_ID`, `part_imp_date`, `part_price`, `part_amt`) VALUES
(433, 305, 'Coller master 200', 201, '2016-04-29 08:00:00', '1200.00', 4),
(432, 305, 'Coller master 100', 201, '2016-04-29 08:00:00', '750.00', 5),
(431, 304, 'Asus 9800gt', 201, '2016-04-29 08:00:00', '3500.00', 5),
(430, 304, 'Asus gtx750 ti oc', 201, '2016-04-29 08:00:00', '5700.00', 5),
(429, 304, 'Asus gtx980', 201, '2016-04-29 08:00:00', '24000.00', 4),
(428, 303, 'Kingston Hyper-X 16GB', 201, '2016-04-29 08:00:00', '3100.00', 5),
(427, 303, 'Kingston Hyper-X 8GB', 201, '2016-04-29 08:00:00', '2400.00', 5),
(426, 303, 'Kingston Hyper-X 4GB', 201, '2016-04-29 08:00:00', '1200.00', 5),
(425, 303, 'Kingston Hyper-X 2GB', 201, '2016-04-29 08:00:00', '800.00', 5),
(424, 302, 'Asus H93m', 201, '2016-04-29 08:00:00', '5100.00', 5),
(423, 302, 'Asus H83m', 201, '2016-04-29 08:00:00', '4200.00', 4),
(422, 301, 'Intel core-i3', 201, '2016-04-29 08:00:00', '3500.00', 4),
(421, 301, 'Intel core-i5', 201, '2016-04-29 08:00:00', '5500.00', 3),
(420, 301, 'Intel core-i7', 201, '2016-04-29 08:00:00', '8500.00', 4),
(434, 305, 'Coller master 400', 201, '2016-04-29 08:00:00', '2100.00', 5),
(435, 306, 'Sound card s100', 201, '2016-04-29 08:00:00', '400.00', 5),
(436, 306, 'Wireless card w100', 201, '2016-04-29 08:00:00', '560.00', 5),
(437, 307, 'Lok 500', 201, '2016-04-29 08:00:00', '1200.00', 4),
(438, 308, 'mouse and keyboard', 201, '2016-04-29 08:00:00', '500.00', 5),
(439, 301, 'Intel core2duo', 202, '2016-04-02 07:00:00', '2500.00', 15);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_suppliers`
--

CREATE TABLE `tbl_suppliers` (
  `sup_ID` int(10) UNSIGNED NOT NULL,
  `sup_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sup_address` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sup_num` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_suppliers`
--

INSERT INTO `tbl_suppliers` (`sup_ID`, `sup_name`, `sup_address`, `sup_num`) VALUES
(201, 'BkkComPart.co', 'Sukhumwit', '020000000'),
(202, 'Adwork.co', 'Bangna', '029999999'),
(205, 'UsComParts', 'NYC', '-');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admins`
--
ALTER TABLE `tbl_admins`
  ADD PRIMARY KEY (`adm_ID`);

--
-- Indexes for table `tbl_builds`
--
ALTER TABLE `tbl_builds`
  ADD KEY `part_ID` (`part_ID`);

--
-- Indexes for table `tbl_categories`
--
ALTER TABLE `tbl_categories`
  ADD PRIMARY KEY (`cat_ID`);

--
-- Indexes for table `tbl_customers`
--
ALTER TABLE `tbl_customers`
  ADD PRIMARY KEY (`cus_ID`);

--
-- Indexes for table `tbl_orders`
--
ALTER TABLE `tbl_orders`
  ADD PRIMARY KEY (`ord_ID`),
  ADD KEY `cus_ID` (`cus_ID`),
  ADD KEY `bu_ID` (`bu_ID`);

--
-- Indexes for table `tbl_parts`
--
ALTER TABLE `tbl_parts`
  ADD PRIMARY KEY (`part_ID`),
  ADD KEY `cat_ID` (`cat_ID`),
  ADD KEY `sup_ID` (`sup_ID`);

--
-- Indexes for table `tbl_suppliers`
--
ALTER TABLE `tbl_suppliers`
  ADD PRIMARY KEY (`sup_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admins`
--
ALTER TABLE `tbl_admins`
  MODIFY `adm_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=704;
--
-- AUTO_INCREMENT for table `tbl_categories`
--
ALTER TABLE `tbl_categories`
  MODIFY `cat_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=310;
--
-- AUTO_INCREMENT for table `tbl_customers`
--
ALTER TABLE `tbl_customers`
  MODIFY `cus_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;
--
-- AUTO_INCREMENT for table `tbl_orders`
--
ALTER TABLE `tbl_orders`
  MODIFY `ord_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=602;
--
-- AUTO_INCREMENT for table `tbl_parts`
--
ALTER TABLE `tbl_parts`
  MODIFY `part_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=440;
--
-- AUTO_INCREMENT for table `tbl_suppliers`
--
ALTER TABLE `tbl_suppliers`
  MODIFY `sup_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=206;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
