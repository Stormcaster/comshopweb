<?php
session_start();
@mysql_connect("localhost","admin","Password") or die (mysql_error());
@mysql_select_db("computershopv0.1") or die (mysql_error());

?>
<?php
$errorMsg = '';
do {
	if(isset($_POST['regisBtn'])) {
		if(empty($_POST['name'])) { $errorMsg = "Please enter your name"; break; }
		if(empty($_POST['surname'])) { $errorMsg = "Please enter your surname"; break; }
		if(empty($_POST['address'])) { $errorMsg = "Please enter your address"; break; }
		if(empty($_POST['num'])) { $errorMsg = "Please enter your phone number"; break; }
		if(empty($_POST['user'])) { $errorMsg = "Please enter your desired user ID"; break; }
		if(empty($_POST['password'])) { $errorMsg = "Please enter desired your password"; break; }
		if(empty($_POST['confirmPassword'])) { $errorMsg = "Please confirm your password"; break; }
		if($_POST['password'] != $_POST['confirmPassword']) { $errorMsg = "Password does not match. Please check again."; break; }

      do_register();
	}
}while(0);
function do_register() {
   $name = $_POST['name'];
   $surname = $_POST['surname'];
   $user = $_POST['user'];
   $password = $_POST['password'];
   $address = $_POST['address'];
   $num = $_POST['num'];
   global $errorMsg;
   if( mysql_num_rows(mysql_query("SELECT * FROM tbl_customers WHERE tbl_customers.cus_user = '$user';") ) == 0 ) {
      $sql = "INSERT INTO `tbl_customers` VALUES (DEFAULT, '$name', '$surname', '$user', '$password', '$address', '$num');";
      mysql_query($sql) or die (mysql_error());
		header('Location: index.php');
   } else {
	   $errorMsg = "Your ID already exists. Please choose a new one.";
   }
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Index</title>
	<link rel="stylesheet"
	href="bootstrap-3.3.6-dist/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="styles/registerStyle.css">
</head>
<body>
	<div class="col-md-offset-3 col-sm-offset-3 col-xs-offset-3">
		<div class="row">

			<div class="container vcenter" id="registerBox">
				<div class="row" id="notifyLbl"><center>Please enter your credentials</center></div>
				<div class="row" id="alertLbl"><center>
					<?php
						if($errorMsg != '')
						echo '<div class="alert alert-danger">'. $errorMsg .'</div>';
					?>
				</center></div>
				<form action="register.php" method="post">
					<div class="form-group">
						<label for="name">Name</label>
						<input type="id" class="form-control" id="name" name="name" placeholder="Name...">
						</div>
						<div class="form-group">
						<label for="surname">Surname</label>
						<input type="id" class="form-control" id="surname" name="surname" placeholder="Surname...">
						</div>
						<div class="form-group">
						<label for="address">Address</label>
						<input type="id" class="form-control" id="address" name="address" placeholder="Address">
						</div>
						<div class="form-group">
						<label for="number">Phone Number</label>
						<input type="id" class="form-control" id="number" name="num" placeholder="Phone No...">
						</div>
						<div class="form-group">
						<label for="user">User ID</label>
						<input type="id" class="form-control" id="user" name="user" placeholder="ID...">
						</div>
						<div class="form-group">
						<label for="password">Password</label>
						<input type="password" class="form-control" id="password" name="password" placeholder="Password...">
						</div>
						<div class="form-group">
						<label for="confirmPassword">Confirm Password</label>
						<input type="password" class="form-control" id="confirmPassword" name="confirmPassword" placeholder="Re-enter Password...">

					</div>
					<button type="submit" class="btn btn-primary" id="regisBtn" name="regisBtn">Register</button>
					<a href="index.php" class="btn btn-danger">Cancel</a>
				</form>

			</div>
		</div>
	</div>
	<script src="jquery-2.2.3.min.js"></script>

</body>
</html>
